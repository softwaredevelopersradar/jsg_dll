﻿using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace JsgSimulator
{
    class Program
    {
        static byte[] request;
        private Semaphore smphForCount;
        static byte bCountChiperSendCmd = 0;

        static void Main(string[] args)
        {
            IPEndPoint local = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 9114);
            UdpClient socket = new UdpClient(local); // `new UdpClient()` to auto-pick port
            // планируем первую операцию приема
            socket.BeginReceive(new AsyncCallback(OnUdpData), socket);
            // отправка данных (для упрощения - на свой же комп): 192.0.102 127.0.0.1
            IPEndPoint target = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 9108); //192.168.0.102
            // отправить пару сообщений для примера:
            //for (int num = 1; num <= 3; num++)
            //{
            //    byte[] message = new byte[num];
            //    socket.Send(message, message.Length, target);
            //}
            Console.ReadKey();
        }


        static void OnUdpData(IAsyncResult result)
        {
            // то, что указано в BeginReceive как 2й параметр:
            UdpClient socket = result.AsyncState as UdpClient;
            // указывает на того, кто отправил сообщение:
            IPEndPoint source = new IPEndPoint(0, 0);
            // получает текущее сообщение и заполняет источник:
            byte[] message = socket.EndReceive(result, ref source);
            // делай, что хочешь с `message` тут:
            //  Console.WriteLine("Got " + Encoding.ASCII.GetString(message) + " bytes from " + source);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Got " + message[2] + " codogram from " + source + ", countCMD = " + message[3]);
            request = SendRequest(message[2], message[0], message[1]);

            socket.Send(request, request.Length, source);
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Send " + request[2] + " codogram to " + source + ", countCMD = " + request[3]);
            //планируем следующую оперцию приема после завершения чтения:
            socket.BeginReceive(new AsyncCallback(OnUdpData), socket);
        }


        static byte[] SendRequest(byte bCipher, byte sender, byte receiver)
        {
            Program p = new Program();
            p.smphForCount = new Semaphore(0, 1);
            byte[] bArreyForSend = new byte[8];
            bArreyForSend[0] = receiver;
            bArreyForSend[1] = sender;
            bArreyForSend[2] = bCipher;
            p.smphForCount.Release();
            bCountChiperSendCmd = Convert.ToByte((bCountChiperSendCmd == 255 ? 0 : bCountChiperSendCmd + 1));
            bArreyForSend[3] = bCountChiperSendCmd;
            p.smphForCount.WaitOne();
            bArreyForSend[4] = Convert.ToByte(bArreyForSend.Length - 5);
            Array.Copy(Enumerable.Repeat((byte)0, 3).ToArray(), 0, bArreyForSend, 5, 3); //для упрощения кидаем код ошибки 0
            return bArreyForSend;
        }
    }
}
