﻿using System;
using System.Collections.Generic;
using PackageJSG;

namespace JsgLib
{
    interface IJsgLib
    {
        bool SetRadiatOff(); // (шифр 10) выключить излучение
        bool SetParamFFJam(FFJammingMessage data);   // (шифр 5) Установить параметры РП для ИРИ ФРЧ и включить излучение заданной длительности
        bool SetParamFHSSJam(FHSSJammingMessage data); // (шифр 18) запрос "Установить параметры РП для ИРИ ППРЧ" (несколько сетей)
        //bool SetParamFFJam(List<FFJamming> listFFJamming, int duration);   // (шифр 5) Установить параметры РП для ИРИ ФРЧ и включить излучение заданной длительности
        //bool SetParamFHSSJam(List<FHSSJamming> listFHSSJamming, int duration, byte codeFFT); // (шифр 18) запрос "Установить параметры РП для ИРИ ППРЧ" (несколько сетей)
        bool StopFHSS(); // (шифр 12) Выход из режима РП-ППРЧ
        bool GetRegimeJSG(); // (шифр 13) запрос состояния ФПС           

        bool SetVoice(VoiceSourceMessage data);
        //bool SetVoice(string filepath);   
        //bool SetVoice(byte numberOfDevice);
        void RegimeVoice(ControlVoiceMessage data);
        //void RegimeVoice(VoiceRegime bMode,int frequency, byte deviation );

        bool PowerOn(); //Включить питание  (шифр 1)
        bool PowerOff(); //Отключить питание (шифр 2)
        bool RadiationOn(); //Включить излучение (шифр 3)
        bool RadiationOff(); //Отключить излучение (шифр 4)
        bool SetPowerLevel(PowerLevelMessage data);  //(шифр 11) Установить уровень мощности
        //bool SetPowerLevel(PowerCode powerCode);  //(шифр 11) Установить уровень мощности
        bool GetState(); // (шифр 24) запрос состояния   
        bool GetFailure(); // (шифр 25) запрос отказов
        bool SetAmplifierFilter(FilterMessage data); //ТУ Фильтр (шифр 32)
        //bool SetAmplifierFilter(Filter filter); //ТУ Фильтр (шифр 32)
        bool GetTelesignalization(); //Запрос Телесигнализации  (шифр 33)

        void Connect();
        void Disconnect();
        
        


        event EventHandler OnConnect; // событие подключния
        event EventHandler OnConnectFailed;
        event EventHandler OnDisconnect; // событие отключения
        event EventHandler<byte[]> OnReceiveBytes; // получен массив байт
        event EventHandler<byte[]> OnSendBytes;// отпрвлен массив байт


        //event EventHandler<OneValEventArgs> UpdateState;
        //event EventHandler<ThreeValEventArgs> UpdatePower;
        //event EventHandler<TwoValEventArgs> UpdateVoltage;
        //event EventHandler<FiveValEventArgs> UpdateCurrent;
        //event EventHandler<FiveValEventArgs> UpdateTemp;
        event EventHandler<StateEventArgs> RecivedStateFPS;

        event EventHandler OnLostAnyCmd; //Утеряна кодограмма

        event EventHandler<RecieveCmdEventArgs> ReceiveCmd;// событие получения команды
        //event EventHandler<byte> SendCmd; // отправлена кодограмма

        event EventHandler OnLostFPS;  //ФПС не отвечает (на речеподобную помеху)

    }
}
