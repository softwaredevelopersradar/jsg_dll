﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using PackageJSG;

namespace JsgLib
{
    public class ReceivedDataProcessing : UDP
    {
        private Thread readThread;  
        private int _amauntOfErrorBytes;  //количество байт на ошибки в ответе от ФПС
        private byte[] _inform;
        private byte[] _codeError;   
        
        //для счетчика кодограм
        protected byte countSendCmd; 
        protected byte countReceiveCmd;
        protected byte countChiperSendCmd;


        protected ReceivedDataProcessing(int myPort, string myIP, int fpsPort, string fpsIP, int amauntOfErrorBytes) 
            : base (myPort, myIP, fpsPort, fpsIP)
        {
            _amauntOfErrorBytes = amauntOfErrorBytes;
            countSendCmd = countReceiveCmd = countChiperSendCmd = 0;
            readThread = new Thread(new ThreadStart(ReceiveData));
            readThread.IsBackground = true;
            readThread.Start();
        }






        public event EventHandler OnLostAnyCmd;//Утеряна кодограмма
        public event EventHandler<byte[]> OnReceiveBytes;



        /// <summary>
        /// Получен стандартный ответ на каманду для ФПС
        /// </summary>
        public event EventHandler<DefaulJsgResponseMessage> OnReceivedJsgCmd;
        protected virtual void ReceivedBaseCmd(EventHandler<DefaulJsgResponseMessage> some_ev, byte bCode, byte[] error)
        {
            var errors = Google.Protobuf.ByteString.CopyFrom(error);
            var e = new DefaulJsgResponseMessage() { CommandCode = bCode, Errors = errors };
            some_ev?.Invoke(this, e);
        }


        /// <summary>
        /// Получен ответ на каманду "Состояние ФПС"
        /// </summary>
        public event EventHandler<StateJsgResponseMessage> OnRecivedStateJSG;
        protected virtual void ReceivedStateJSG(EventHandler<StateJsgResponseMessage> some_ev, byte bCode, byte[] error, byte[] bInform)
        {
            var errors = Google.Protobuf.ByteString.CopyFrom(error);

            var e = new StateJsgResponseMessage() { JsgError = new DefaulJsgResponseMessage() { CommandCode = bCode, Errors = errors }, Mode = (JsgMode)bInform[0] };
            for (int i=1; i< bInform.Length; i+=3)
            {
                e.FirmwareVersion.Add(Google.Protobuf.ByteString.CopyFrom(bInform, i, 3));
            }
            some_ev?.Invoke(this, e);
        }


        /// <summary>
        /// Получен стандартный ответ на каманду для Усилителей
        /// </summary>
        public event EventHandler<DefaulAmResponseMessage> OnReceivedAmCmd;
        protected virtual void ReceivedAMCmd(EventHandler<DefaulAmResponseMessage> some_ev, byte bCode, byte[] error, byte[] bInform)
        {
            var errors = Google.Protobuf.ByteString.CopyFrom(error);
            var A1A2 = new BitArray(new byte[] { bInform[1] });
            var alarmInfo = new AlarmMessage() { AmplifierAlarm = A1A2.Get(0), FiltersAlarm = A1A2.Get(1), PowerAlarm = A1A2.Get(3) }; //TODO: проверить порядок байт, мб не с нуля, а с конца брать
            var e = new DefaulAmResponseMessage() { JsgError = new DefaulJsgResponseMessage() { CommandCode = bCode, Errors = errors }, Result = (CommandResult)bInform[0], AlarmInfo = alarmInfo };

            some_ev?.Invoke(this, e);
        }

        /// <summary>
        /// Получен ответ на каманду на «Запрос состояния» усилителей
        /// </summary>
        public event EventHandler<StateAmResponseMessage> OnReceivedStateAM;
        protected virtual void ReceivedStateAMCmd(EventHandler<StateAmResponseMessage> some_ev, byte bCode, byte[] error, byte[] bInform)
        {
            var errors = Google.Protobuf.ByteString.CopyFrom(error);
            var A1A2 = new BitArray(new byte[] { bInform[0] });

            //var temperatue = ((bInform[1] & 0xF0) >> 4) * 10 + (bInform[1] & 0x0F); //TODO: Check
            var temperatue = (bInform[1] & 0x0F) * 10 + (bInform[1] & 0xF0) >> 4; //TODO: Check
            var vswr = ((bInform[2] & 0xF0) >> 4) * 0.1f + (bInform[2] & 0x0F); //TODO: Check
            var power = ((bInform[3] & 0xF0) >> 4) * 100 + (bInform[3] & 0x0F) * 1000 + ((bInform[4] & 0xF0) >> 4) + (bInform[4] & 0x0F) * 10; //TODO: Check

            var alarmInfo = new AlarmMessage() { AmplifierAlarm = A1A2.Get(0), FiltersAlarm = A1A2.Get(1), PowerAlarm = A1A2.Get(3) };//TODO: проверить порядок байт, мб не с нуля, а с конца брать
            var e = new StateAmResponseMessage() { JsgError = new DefaulJsgResponseMessage() { CommandCode = bCode, Errors = errors },
                HasPower = A1A2.Get(0), HasRadiation = A1A2.Get(1), IsLocalControl = A1A2.Get(2), Setted2Filter = A1A2.Get(3), HasFailure = A1A2.Get(4), IsReady = A1A2.Get(5), PowerExceeds500Vt = A1A2.Get(6),
                Temperature = temperatue, VSWR = vswr, Power = power
            };

            some_ev?.Invoke(this, e);
        }

        /// <summary>
        /// Получен ответ на каманду на «Запрос отказов» усилителей
        /// </summary>
        public event EventHandler<FailureAmResponseMessage> OnReceivedFailureAm;
        protected virtual void ReceivedAMFailure(EventHandler<FailureAmResponseMessage> some_ev, byte bCode, byte[] error, byte[] bInform)
        {
            var errors = Google.Protobuf.ByteString.CopyFrom(error);
            var A1A2 = new BitArray(88);
            bool[] myBoolArray = new bool[bInform.Length*8];
            for (int i = 0; i < bInform.Length; i++)
            {
                var x1x2 = new BitArray(new byte[] { bInform[i] });
                Reverse(x1x2); //TODO: check
                x1x2.CopyTo(myBoolArray, i*8);
            }
            List<byte> failureBitsList = new List<byte>();

            var e = new FailureAmResponseMessage()
            {
                JsgError = new DefaulJsgResponseMessage() { CommandCode = bCode, Errors = errors }
            };
          
            for (int i = 0; i < myBoolArray.Length; i++)
            {
                if (myBoolArray[i])
                {
                    failureBitsList.Add((byte)(i + 1));
                }
            }
            e.FailureBitsNums = Google.Protobuf.ByteString.CopyFrom(failureBitsList.ToArray());
            some_ev?.Invoke(this, e);
        }

        /// <summary>
        /// Получен ответ на каманду «Запрос Телесигнализации» для Усилителей
        /// </summary>
        public event EventHandler<TelesignalResponseMessage> OnReceivedTelesignalAmCmd;
        protected virtual void ReceivedTelesignalAMCmd(EventHandler<TelesignalResponseMessage> some_ev, byte bCode, byte[] error, byte[] bInform)
        {
            var errors = Google.Protobuf.ByteString.CopyFrom(error);
            var e = new TelesignalResponseMessage()
            {
                JsgError = new DefaulJsgResponseMessage() { CommandCode = bCode, Errors = errors }, Failure = Convert.ToBoolean(bInform[0]), IsReady = Convert.ToBoolean(bInform[1]), IsWorking = Convert.ToBoolean(bInform[2])
            };

            some_ev?.Invoke(this, e);
        }

        private BitArray Reverse(BitArray array)
        {
            int length = array.Length;
            int mid = (length / 2);

            for (int i = 0; i < mid; i++)
            {
                bool bit = array[i];
                array[i] = array[length - i - 1];
                array[length - i - 1] = bit;
            }

            return array;
        }


        //Проверка пришедших данных на наличие кодограммы
        private void ReceiveData()
        {
            while (true)
            {
                if (dataReceivedFlag)
                {
                    OnReceiveBytes?.Invoke(this, dataRead);
                    if (dataRead.Length >= 5)
                    {
                        byte bCode = dataRead[2];  // шифр кодограммы
                        if (Enum.IsDefined(typeof(CodogramCode), bCode)) 
                        {
                            FillingForTCodeErrorInform(bCode, dataRead);
                            CheckData(bCode);
                        }
                    }
                    dataReceivedFlag = false;
                }
                else
                {
                    Thread.Sleep(100);
                }
            }
        }

        //проверка обновлений параметров литеры
        private void CheckData(byte bCode)
        {
            switch (bCode)
            {
                case (byte)CodogramCode.POWER_ON:
                case (byte)CodogramCode.POWER_OFF:
                case (byte)CodogramCode.RADIATION_ON:
                case (byte)CodogramCode.RADIATION_OFF:     
                case (byte)CodogramCode.SET_POWER_LVL:
                case (byte)CodogramCode.SET_AMP_FILTER:
                    ReceivedAMCmd(OnReceivedAmCmd,  bCode, _codeError, _inform);
                    break;
                case (byte)CodogramCode.DURAT_PARAM_FWS:
                case (byte)CodogramCode.RADIAT_OFF:
                case (byte)CodogramCode.STOP_FHSS:                
                case (byte)CodogramCode.CONFIRM_SLI:
                case (byte)CodogramCode.SEND_SLI:
                case (byte)CodogramCode.PARAM_FHSS_MANY:
                    ReceivedBaseCmd(OnReceivedJsgCmd, bCode, _codeError);
                    break;
                case (byte)CodogramCode.GET_REGIME_JSG:
                    ReceivedStateJSG(OnRecivedStateJSG, bCode, _codeError, _inform);
                    break;
                case (byte)CodogramCode.GET_STATE:
                    ReceivedStateAMCmd(OnReceivedStateAM, bCode, _codeError, _inform);
                    break;
                case (byte)CodogramCode.GET_FAILURE:
                    ReceivedAMFailure(OnReceivedFailureAm, bCode, _codeError, _inform);
                    break;
                case (byte)CodogramCode.GET_TELESIGNALIZATION:
                    ReceivedTelesignalAMCmd(OnReceivedTelesignalAmCmd, bCode, _codeError, _inform);
                    break;
                default:
                    break;
            }
        }

        //чтение ошибки и доп.инфы в кодограмме от ФПС 
        private void FillingForTCodeErrorInform(byte NumberOfCMD, byte[] data)
        {
            _inform = data.Length > (5 + _amauntOfErrorBytes) ? new byte[data.Length - (5 + _amauntOfErrorBytes)] : null;

            _codeError = new byte[_amauntOfErrorBytes]; // код ошибки
            try
            { Array.Copy(data, 5, _codeError, 0, _amauntOfErrorBytes); }
            catch {  }
            if (data.Length > 5 + _amauntOfErrorBytes)
                Array.Copy(data, 5 + _amauntOfErrorBytes, _inform, 0, data.Length - (5 + _amauntOfErrorBytes));
            //CatchesLosses();
        }

        //Проверка кол-ва пакетов на наличие потерь
        private void CatchesLosses()
        {
            countReceiveCmd = Convert.ToByte((countReceiveCmd == 255 ? 0 : countReceiveCmd + 1));
            if (countReceiveCmd != countSendCmd) 
            {
                OnLostAnyCmd?.Invoke(this, null); 
                countReceiveCmd = 0;
                countSendCmd = 0;
            }
        }

    }


}
