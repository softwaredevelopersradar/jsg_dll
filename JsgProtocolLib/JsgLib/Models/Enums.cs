﻿using System.ComponentModel;

namespace JsgLib
{
    //public enum PowerCode : byte
    //{
    //    [Description("100%")]
    //    P_100 = 0,
    //    [Description("50%")]
    //    P_50,
    //    [Description("25%")]
    //    P_25
    //}


    //public enum Filter : byte
    //{
    //    [Description("1.5-18.8 MHz")]
    //    Low = 0,
    //    [Description("18.9-30 MHz")]
    //    High
    //}


    //public enum VoiceRegime : byte
    //{
    //    Stop = 0,
    //    Start
    //}


    public enum CodogramCode : byte
    {
        POWER_ON = 1,
        POWER_OFF,
        RADIATION_ON,
        RADIATION_OFF,
        DURAT_PARAM_FWS,
        RADIAT_OFF = 10,
        SET_POWER_LVL,
        STOP_FHSS = 12,
        GET_REGIME_JSG,
        CONFIRM_SLI = 16,
        SEND_SLI,
        PARAM_FHSS_MANY,
        GET_STATE = 24,
        GET_FAILURE,
        SET_AMP_FILTER = 32,
        GET_TELESIGNALIZATION
    }


    //public enum CommandResult : byte 
    //{
    //    Executed = 0x0,
    //    Alarm = 0xAB,
    //    RemoteControlError = 0x0A,
    //    TypingError = 0x0B,
    //    LocalControl = 0xAA
    //}
}
