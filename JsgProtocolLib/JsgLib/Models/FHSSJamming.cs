﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JsgLib
{
    public struct FHSSJamming
    {
        public double FreqMinKHz { get; set; }
        public byte Modulation { get; set; }
        public byte Deviation { get; set; }
        public byte Manipulation { get; set; }

        public FHSSJamming(double frequencyMinKHz, byte modulation, byte deviation, byte manipulation)
        {
            FreqMinKHz = frequencyMinKHz;
            Modulation = modulation;
            Deviation = deviation;
            Manipulation = manipulation;
        }
    }
}
