﻿namespace JsgLib
{
    public struct FFJamming
    {
        public double FreqKHz { get; private set; }
        public byte Modulation { get; private set; }
        public byte Deviation { get; private set; }
        public byte Manipulation { get; private set; }
        public byte Duration { get; private set; }

        public FFJamming(double frequency, byte modulation, byte deviation, byte manipulation, byte duration)
        {
            FreqKHz = frequency;
            Modulation = modulation;
            Deviation = deviation;
            Manipulation = manipulation;
            Duration = duration;
        }

        //public FFJamming(byte[] bytes, int startIndex = 0)
        //{
        //    try
        //    {
        //        Letter = bytes[startIndex];
        //        Voltage = BitConverter.ToUInt16(bytes, startIndex + 1);
        //    }
        //    catch
        //    {
        //        Letter = 0;
        //        Voltage = 0;
        //    }
        //}
    }
}
