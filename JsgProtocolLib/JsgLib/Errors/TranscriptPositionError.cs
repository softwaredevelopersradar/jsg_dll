﻿using System;
using System.Collections.Generic;
using System.Collections;

namespace WAVtranslation
{
    class TranscriptPositionError
    {
        int amountOfLetter;//доработать
        string[,] AllVersionForFirstByteError = new string[2, 8] {
            {"7 литер","не превышено кол-во частот","частота принадлежит диапозону","корректный код модуляции","корректный код девиации","корректный код манипуляции","длительность принадлежит диапозону","отсутствует аппаратная неопределенная"},
            {"9 литер","превышено кол-во частот в литере/-ах","частота не принадлежит диапозону","некорректный код модуляции","некорректный код девиации","некорректный код манипуляции","длительность не принадлежит диапозону","аппаратная неопределенная"}};
        string[] NumberOfLeters = new string[7] { "1/3", "2/4", "5", "6", "7", "8", "9" };

        Dictionary<byte, string> DictionaryOfFirstByteError;
        Dictionary<byte, string> DictionaryOfSecondByteError;
        Dictionary<byte, string> DictionaryOfThirdByteError;

        public TranscriptPositionError()
        {
            DictionaryOfFirstByteError = new Dictionary<byte, string>();
            DictionaryOfSecondByteError = new Dictionary<byte, string>();
            DictionaryOfThirdByteError = new Dictionary<byte, string>();
            for (int i = 0; i < 256; i++)//для первого байта ошибок
            {
                string sTranscriptionForThisNumber = "";
                byte[] bForTranskription = new byte[] { Convert.ToByte(i) };
                BitArray btForTranskription = new BitArray(bForTranskription);
                for (int j = 7; j >= 0; j--)
                {
                    int k = (btForTranskription[j] ? 1 : 0);
                    sTranscriptionForThisNumber = String.Concat(sTranscriptionForThisNumber, AllVersionForFirstByteError[k, 7 - j], "\r\n");//проверить, в битаррей обратный порядок байт
                }
                DictionaryOfFirstByteError.Add(Convert.ToByte(i), sTranscriptionForThisNumber);
            }

            //для второго
            DictionaryOfSecondByteError.Add(0, "Есть ответы от синтезаторов всех литер\r\n");
            for (int i = 1; i < 128; i++)
            {
                String sTranscriptionForThisNumber = "Нет ответа от синтезатора литеры ";
                byte[] bForTranskription = new byte[] { Convert.ToByte(i) };
                BitArray btForTranskription = new BitArray(bForTranskription);
                for (int j = 0; j < 7; j++)
                    if (btForTranskription[j])
                        sTranscriptionForThisNumber = String.Concat(sTranscriptionForThisNumber, NumberOfLeters[j], ", ");
                sTranscriptionForThisNumber = sTranscriptionForThisNumber.Remove(sTranscriptionForThisNumber.Length - 2);
                sTranscriptionForThisNumber = String.Concat(sTranscriptionForThisNumber, "\r\n");
                DictionaryOfSecondByteError.Add(Convert.ToByte(i), sTranscriptionForThisNumber);
            }

            //для третьего
            DictionaryOfThirdByteError.Add(0, "Есть связь с усилителями мощности всех литер\r\n");
            for (int i = 1; i < 128; i++)
            {
                String sTranscriptionForThisNumber = "Нет связи с усилителем мощности синтезатора литеры ";
                byte[] bForTranskription = new byte[] { Convert.ToByte(i) };
                BitArray btForTranskription = new BitArray(bForTranskription);
                for (int j = 0; j < 7; j++)
                    if (btForTranskription[j])
                        sTranscriptionForThisNumber = String.Concat(sTranscriptionForThisNumber, NumberOfLeters[j], ", ");
                sTranscriptionForThisNumber = sTranscriptionForThisNumber.Remove(sTranscriptionForThisNumber.Length - 2);
                sTranscriptionForThisNumber = String.Concat(sTranscriptionForThisNumber, "\r\n");
                DictionaryOfThirdByteError.Add(Convert.ToByte(i), sTranscriptionForThisNumber);
            }
        }

        public bool IsNotThereError(byte[] InputData)
        {
            int resForFirstByte = 1;
            byte[] forTranskription = new byte[] { InputData[0] };
            BitArray btForTranskription = new BitArray(forTranskription);
            for (int i = 6; i >= 0; i--)
            {
                int k = (btForTranskription[i] ? 0 : 1);
                resForFirstByte *= k;
            }
            //return (resForFirstByte == 1 && InputData[1] == 0 && InputData[2] == 0 ? true : false);
            return (resForFirstByte == 1 && InputData[1] == 0 ? true : false);
        }

        public string TranscriptionOfError(byte[] InputData)
        {
            string transcription = string.Concat(DictionaryOfFirstByteError[InputData[0]], DictionaryOfSecondByteError[InputData[1]], DictionaryOfThirdByteError[InputData[2]]); ;
            return transcription;
        }

        //ф-ия распоснозавания проблем с ответом от синтезатора формирователя литеры
        public bool IsThereProblemWithSynthesizer(byte[] ErrorInfo)
        {
            return (ErrorInfo[1] != 0 ? true : false);
        }

        //ф-ия распоснозавания номера литеры от, у которой нет связи c усилителем мощности лиеры
        public string[] WhichProblemWithSynthesizer(byte[] ErrorInfo)
        {
            string[] res = null;
            if (ErrorInfo[1] != 0)
            {
                string sAnswer = DictionaryOfSecondByteError[ErrorInfo[1]];
                sAnswer = sAnswer.Replace("Нет ответа от синтезатора литеры ", "");
                sAnswer = sAnswer.Replace(", ", " ");
                sAnswer = sAnswer.Replace("\r\n", "");
                res = sAnswer.Split(' ');
            }
            return res;
        }

        //ф-ия распоснозавания проблем со связью c усилителем мощности лиеры
        public bool IsThereLinkWithAmplifaer(byte[] ErrorInfo)
        {
            return (ErrorInfo[2] != 0 ? true : false);
        }

        public string[] WhichLinkProblemAmplifaersAnswer(byte[] ErrorInfo)
        {
            string[] res = null;
            if (ErrorInfo[2] != 0)
            {
                string sAnswer = DictionaryOfThirdByteError[ErrorInfo[1]];
                sAnswer = sAnswer.Replace("Нет связи с усилителем мощности синтезатора литеры ", "");
                sAnswer = sAnswer.Replace(", ", " ");
                sAnswer = sAnswer.Replace("\r\n", "");
                res = sAnswer.Split(' ');
            }
            return res;
        }
    }
}
