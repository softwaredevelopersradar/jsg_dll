﻿using System;
using System.Collections.Generic;
using System.Collections;

namespace WAVtranslation
{
    class TranscriptOfErrors
    {
        const byte duratParamFWS = 5;
        const byte paramFHSS = 11;
        static string[] sForCombination = new string[] { "1/3", "2/4", "5", "6", "7" };//пока случай для 7
        Dictionary<byte, string> DictionaryOfCommandError;
        Dictionary<byte, string> DictionaryOfUniqueErrorForFWS;
        Dictionary<byte, string> DictionaryOfUniqueErrorForFHSS;
        
        public TranscriptOfErrors()
        {
            DictionaryOfCommandError = new Dictionary<byte, string>();
            DictionaryOfCommandError.Add(0, "верно");
            DictionaryOfCommandError.Add(1, "программная неопределенная");
            DictionaryOfCommandError.Add(20, "аппаратная неопределенная");
            
            DictionaryOfCommandError.Add(7, "превышено кол-во частот в литере 6");
            DictionaryOfCommandError.Add(8, "превышено кол-во частот в литере 7");
            DictionaryOfCommandError.Add(9, "значение частоты вне диапазона");
            DictionaryOfCommandError.Add(10, "значение длительности вне диапазона");
            DictionaryOfCommandError.Add(11, "неверный код модуляции ");
            DictionaryOfCommandError.Add(12, "неверный код девиации");
            DictionaryOfCommandError.Add(13, "неверный код манипуляции");
            
            for (int i1 = 33; i1 <= 63; i1++)
            {
                string noAnswerFromSynthesizer = "нет ответа от синтезатора формирователя литеры";
                byte[] iNumberForDescription = new byte[] { Convert.ToByte(i1 - 32) };
                BitArray btForFormula = new BitArray(iNumberForDescription);
                for (int i2 = 0; i2 < 5; i2++)
                    if (btForFormula[i2] == true)
                        noAnswerFromSynthesizer = string.Concat(noAnswerFromSynthesizer, " ", sForCombination[i2]);
                DictionaryOfCommandError.Add(Convert.ToByte(i1), noAnswerFromSynthesizer);
            }
            for (int i1 = 65; i1 <= 95; i1++)
            {
                string noConnectionToThePowerAmplifier = "нет связи с усилителем мощности литеры";
                byte[] iNumberForDescription = new byte[] { Convert.ToByte(i1 - 64) };
                BitArray btForFormula = new BitArray(iNumberForDescription);
                for (int i2 = 0; i2 < 5; i2++)
                    if (btForFormula[i2] == true)
                        noConnectionToThePowerAmplifier = string.Concat(noConnectionToThePowerAmplifier, " ", sForCombination[i2]);
                DictionaryOfCommandError.Add(Convert.ToByte(i1), noConnectionToThePowerAmplifier);

            }

            DictionaryOfUniqueErrorForFWS = new Dictionary<byte, string>();
            DictionaryOfUniqueErrorForFWS.Add(2, "превышено кол-во частот в литере 1");
            DictionaryOfUniqueErrorForFWS.Add(3, "превышено кол-во частот в литере 2");
            DictionaryOfUniqueErrorForFWS.Add(4, "превышено кол-во частот в литере 3");
            DictionaryOfUniqueErrorForFWS.Add(5, "превышено кол-во частот в литере 4");
            DictionaryOfUniqueErrorForFWS.Add(6, "превышено кол-во частот в литере 5");
            
            DictionaryOfUniqueErrorForFHSS = new Dictionary<byte, string>();
            DictionaryOfUniqueErrorForFHSS.Add(2, "значение мин. частоты вне диапазона");
            DictionaryOfUniqueErrorForFHSS.Add(3, "значение макс. частоты вне диапазона");
            DictionaryOfUniqueErrorForFHSS.Add(4, "неверный код модуляции ");
            DictionaryOfUniqueErrorForFHSS.Add(5, "неверный код девиации");
            DictionaryOfUniqueErrorForFHSS.Add(6, "неверный код манипуляции");
        }

        //ф-ия расшифровки
        public string DecodingError(byte[] ErrorInfo)//bCode, byte bError
        {
            if (DictionaryOfCommandError.ContainsKey(ErrorInfo[1]))
                return DictionaryOfCommandError[ErrorInfo[1]];
            else if (ErrorInfo[0] == duratParamFWS && DictionaryOfUniqueErrorForFWS.ContainsKey(ErrorInfo[1]))
                return DictionaryOfUniqueErrorForFWS[ErrorInfo[1]];
            else if (ErrorInfo[0] == paramFHSS && DictionaryOfUniqueErrorForFHSS.ContainsKey(ErrorInfo[1]))
                return DictionaryOfUniqueErrorForFHSS[ErrorInfo[1]];
            else
                return "Unknown error number";
        }

        public bool IsNotThereError(byte[] ErrorInfo)
        {
            return (ErrorInfo[1] == 0 || (ErrorInfo[1] >= 65 && ErrorInfo[1] <= 95) ? true : false); 
        }

        //ф-ия распоснозавания проблем с ответом от синтезатора формирователя литеры
        public bool IsThereProblemWithSynthesizer(byte[] ErrorInfo)
        {
            return (ErrorInfo[1] >= 33 && ErrorInfo[1] <= 63 ? true : false);
        }

        //ф-ия распоснозавания номера литеры от, у которой нет связи c усилителем мощности лиеры
        public string[] WhichProblemWithSynthesizer(byte[] ErrorInfo)
        {
            string[] res = null;
            if (ErrorInfo[1] >= 33 && ErrorInfo[1] <= 63)
            {
                string sAnswer = DictionaryOfCommandError[ErrorInfo[1]];
                sAnswer = sAnswer.Replace("нет ответа от синтезатора формирователя литеры ", "");
                res = sAnswer.Split(' ');
            }
            return res;
        }

        //ф-ия распоснозавания проблем со связью c усилителем мощности лиеры
        public bool IsThereLinkWithAmplifaer(byte[] ErrorInfo)
        {
            return (ErrorInfo[1] >= 65 && ErrorInfo[1] <= 95 ? true : false);
        }

        public string[] WhichLinkProblemAmplifaersAnswer(byte[] ErrorInfo)
        {
            string[] res = null;
            if (ErrorInfo[1] >= 65 && ErrorInfo[1] <= 95)
            {
                string sAnswer = DictionaryOfCommandError[ErrorInfo[1]];
                sAnswer = sAnswer.Replace("нет связи с усилителем мощности литеры ", "");
                res = sAnswer.Split(' ');
            }
            return res;
        }
    }
}
