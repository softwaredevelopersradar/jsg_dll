﻿namespace WAVtranslation
{
    public class DecodingAllTypesError
    {
        private readonly int _amountOfBytesForDecoding;
        private TranscriptOfErrors _unpositionalError;
        private TranscriptPositionError _positionalError;

        public DecodingAllTypesError(int AmountOfBytesForDecoding)
        {
            _amountOfBytesForDecoding = AmountOfBytesForDecoding;//првить, создавать только один объект
            _unpositionalError = new TranscriptOfErrors();
            _positionalError = new TranscriptPositionError();
        }
        public string DecodingError(byte[] error)
        {
            if (error.Length == 2)
                return _unpositionalError.DecodingError(error);
            else if (error.Length == 3)
                return _positionalError.TranscriptionOfError(error);
            else
                return null;
        }
        public bool IsNotThereError(byte[] error)
        {
            if (error.Length == 2)
                return _unpositionalError.IsNotThereError(error);
            else if (error.Length == 3)
                return _positionalError.IsNotThereError(error);
            else
                return false;
        }
    }
}
