﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JsgLib
{
    public struct AlarmStruct
    {
        public bool AmplifierAlarm { get; private set; }
        public bool FiltersAlarm { get; private set; }
        public bool PowerAlarm { get; private set; }

        public AlarmStruct(bool amplifierAlarm, bool filtersAlarm, bool powerAlarm)
        {
            AmplifierAlarm = amplifierAlarm;
            FiltersAlarm = filtersAlarm;
            PowerAlarm = powerAlarm;
        }

        //public AlarmStruct(byte alarm)
        //{
        //    AmplifierAlarm = amplifierAlarm;
        //    FiltersAlarm = filtersAlarm;
        //    PowerAlarm = powerAlarm;
        //}
    }
}
