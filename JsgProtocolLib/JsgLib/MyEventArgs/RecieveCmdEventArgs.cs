﻿
namespace JsgLib
{
    public struct RecieveCmdEventArgs
    {
        public byte CommandCode { get; private set; }
        public ErrorDataStructure ErrorDataStructure { get; private set; }
        //public CommandResult? CommandResult { get; private set; }
        //public byte? Alarm { get; private set; }
        //public RecieveCmdEventArgs(byte commandCode, ErrorDataStructure errorDataStructure, CommandResult? commandResult, byte? alarm)
        //{
        //    CommandCode = commandCode;
        //    ErrorDataStructure = errorDataStructure;
        //    CommandResult = commandResult;
        //    Alarm = alarm;
        //}
    }
}