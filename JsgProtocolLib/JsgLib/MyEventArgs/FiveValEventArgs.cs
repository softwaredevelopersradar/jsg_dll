﻿using System.Collections.Generic;

namespace JsgLib
{
    public struct FiveValEventArgs
    {
        public ErrorDataStructure Errors;
        public List<FiveParam> FiveParams;
        public FiveValEventArgs(ErrorDataStructure errors, List<FiveParam> fiveParams)
        {
            Errors = errors;
            FiveParams = fiveParams;
        }
    }
}