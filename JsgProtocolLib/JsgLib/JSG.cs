﻿using System;
using System.Collections.Generic;
using NAudio.Wave;
using PackageJSG;

namespace JsgLib
{
    public class JSG : Sound //, IJsgLib
    {
        public int OffsetFreqFHSS { get; set; } = 10000; //для ППРЧ отнимать от min частоты


        public JSG(int myPort, string myIP, int fpsPort, string fpsIP, int iAmauntOfErrorBytes) 
            : base(myPort, myIP, fpsPort, fpsIP, iAmauntOfErrorBytes)
        { }



        #region Old commands

        /// <summary>
        /// Выключить излучение
        /// </summary>
        public bool SetJsgRadiationOff()
        {
            return CommanPartForCodograms(5, (byte)CodogramCode.RADIAT_OFF, null, null);
        }

        /// <summary>
        /// Установить параметры РП для ИРИ ФРЧ и включить излучение заданной длительности
        /// </summary>
        public bool SetParamFFJam(FFJammingMessage data)
        {
            try
            {
                return CommanPartForCodograms(8 + data.Params.Count * 7, (byte)CodogramCode.DURAT_PARAM_FWS,
                    delegate (ref byte[] ForSend)
                    {
                        Array.Copy(BitConverter.GetBytes(data.Duration * 10), 0, ForSend, 5, 3);
                        for (int i = 0; i < data.Params.Count; i++)
                        {
                            Array.Copy(BitConverter.GetBytes((int)data.Params[i].FrequencyKhz), 0, ForSend, i * 7 + 8, 3);
                            ForSend[i * 7 + 11] = (byte)data.Params[i].InterferenceParams.Modulation;
                            ForSend[i * 7 + 12] = (byte)data.Params[i].InterferenceParams.Deviation;
                            ForSend[i * 7 + 13] = (byte)data.Params[i].InterferenceParams.Manipulation;
                            ForSend[i * 7 + 14] = (byte)data.Params[i].InterferenceParams.Duration;
                        }
                    },
                    data.Params);
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        ///// <summary>
        ///// Установить параметры РП для ИРИ ФРЧ и включить излучение заданной длительности
        ///// </summary>
        //public bool SetParamFFJam(List<FFJamming> listFFJamming, int duration)
        //{
        //    try
        //    {
        //        return CommanPartForCodograms(8 + listFFJamming.Count * 7, (byte)CodogramCode.DURAT_PARAM_FWS,
        //            delegate (ref byte[] ForSend)
        //            {
        //                Array.Copy(BitConverter.GetBytes(duration * 10), 0, ForSend, 5, 3); 
        //                for (int i = 0; i < listFFJamming.Count; i++)
        //                {
        //                    Array.Copy(BitConverter.GetBytes((int)listFFJamming[i].FreqKHz), 0, ForSend, i * 7 + 8, 3);
        //                    ForSend[i * 7 + 11] = listFFJamming[i].Modulation;
        //                    ForSend[i * 7 + 12] = listFFJamming[i].Deviation;
        //                    ForSend[i * 7 + 13] = listFFJamming[i].Manipulation;
        //                    ForSend[i * 7 + 14] = listFFJamming[i].Duration;
        //                }
        //            },
        //            listFFJamming);
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //}


        /// <summary>
        /// Установить параметры РП для ИРИ ППРЧ (несколько сетей)
        /// </summary>
        public bool SetParamFHSSJam(FHSSJammingMessage data)
        {
            return CommanPartForCodograms(9 + data.Params.Count * 6, (byte)CodogramCode.PARAM_FHSS_MANY,
               delegate (ref byte[] ForSend)
               {
                   Array.Copy(BitConverter.GetBytes(data.Duration * 10), 0, ForSend, 5, 3);
                   ForSend[8] = (byte)data.CodeFFT;
                   for (int i = 0; i < data.Params.Count; i++)
                   {
                       Array.Copy(BitConverter.GetBytes((int)data.Params[i].FrequencyKhz - OffsetFreqFHSS), 0, ForSend, 6 * i + 9, 3);
                       ForSend[6 * i + 12] = (byte)data.Params[i].InterferenceParams.Modulation;
                       ForSend[6 * i + 13] = (byte)data.Params[i].InterferenceParams.Deviation;
                       ForSend[6 * i + 14] = (byte)data.Params[i].InterferenceParams.Manipulation;
                   }
               },
               data.Params);
        }

        ///// <summary>
        ///// Установить параметры РП для ИРИ ППРЧ (несколько сетей)
        ///// </summary>
        //public bool SetParamFHSSJam(List<FHSSJamming> listFHSSJamming, int duration, byte codeFFT)
        //{
        //    return CommanPartForCodograms(9 + listFHSSJamming.Count * 6, (byte)CodogramCode.PARAM_FHSS_MANY,
        //       delegate (ref byte[] ForSend)
        //       {
        //           Array.Copy(BitConverter.GetBytes(duration * 10), 0, ForSend, 5, 3);
        //           ForSend[8] = codeFFT;
        //           for (int i = 0; i < listFHSSJamming.Count; i++)
        //           {
        //               Array.Copy(BitConverter.GetBytes((int)listFHSSJamming[i].FreqMinKHz - OffsetFreqFHSS), 0, ForSend, 6 * i + 9, 3);
        //               ForSend[6 * i + 12] = listFHSSJamming[i].Modulation;
        //               ForSend[6 * i + 13] = listFHSSJamming[i].Deviation;
        //               ForSend[6 * i + 14] = listFHSSJamming[i].Manipulation;
        //           }
        //       },
        //       listFHSSJamming);
        //}


        /// <summary>
        /// Выход из режима РП-ППРЧ
        /// </summary>
        public bool StopFHSS()
        {
            return CommanPartForCodograms(5, (byte)CodogramCode.STOP_FHSS, null, null);
        }


        /// <summary>
        /// Запрос состояния ФПС
        /// </summary>
        public bool GetRegimeJSG()
        {
            return CommanPartForCodograms(5, (byte)CodogramCode.GET_REGIME_JSG, null, null);
        }


        /// <summary>
        /// Выбор источника голосовой помехи и проверка наличия 
        /// </summary>
        public bool SetVoice(VoiceSourceMessage source)
        {
            switch(source.SourceCase)
            {
                case VoiceSourceMessage.SourceOneofCase.None:
                    return false;
                case VoiceSourceMessage.SourceOneofCase.Filepath:
                    return SetVoice(source.Filepath);
                case VoiceSourceMessage.SourceOneofCase.DeviceNumber:
                    return SetVoice((byte)source.DeviceNumber);
                default:
                    throw new ArgumentException("Unknown instrument type");
            }
        }


        /// <summary>
        /// Проверка наличия файла по заданному пути
        /// </summary>
        private bool SetVoice(string filepath)
        {
            if (System.IO.File.Exists(filepath))
            {
                byte[] fullfile = System.IO.File.ReadAllBytes(filepath);
                file = new byte[fullfile.Length - 44];
                Array.Copy(fullfile, 44, file, 0, fullfile.Length - 44);
                position = 0;
                bOnlineORRecord = 0;
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// Проверка наличия записывающего устройства с заданным номером
        /// </summary>
        private bool SetVoice(byte numberOfDevice)
        {
            try
            {
                var tr = WaveIn.GetCapabilities(numberOfDevice);
                this.numberOfDevice = numberOfDevice;
                bOnlineORRecord = 1;
                return true;
            }
            catch
            {
                return false;
            }
        }



        /// <summary>
        /// Если SetVoice ответил true, передать частоту вещания помехи и режим (1 – включить, 0 – выключить помеху)
        /// </summary>
        public void RegimeVoice(ControlVoiceMessage data)
        {
            SendConfirmation((byte)data.Regime, data.FrequencyKhz, (byte)data.Deviation);
            if (data.Regime == PackageJSG.VoiceRegime.Start)
            {
                StartSendVoice();
            }
            else
            {
                PauseVoice();
            }
        }


        ///// <summary>
        ///// Если SetVoice ответил true, передать частоту вещания помехи и режим (1 – включить, 0 – выключить помеху)
        ///// </summary>
        //private void RegimeVoice(VoiceRegime bMode, int frequencyKHz, byte deviation)
        //{
        //    SendConfirmation((byte)bMode, frequencyKHz, deviation);  
        //    if (bMode == VoiceRegime.Start)
        //    {
        //        StartSendVoice();
        //    }
        //    else
        //    {
        //        PauseVoice();
        //    }
        //}

        #endregion



        #region New commands AM

        /// <summary>
        /// Включить питание
        /// </summary>
        public bool PowerOn()
        {
            return CommanPartForCodograms(5, (byte)CodogramCode.POWER_ON, null, null);
        }


        /// <summary>
        /// Отключить питание
        /// </summary>
        public bool PowerOff()
        {
            return CommanPartForCodograms(5, (byte)CodogramCode.POWER_OFF, null, null);
        }


        /// <summary>
        /// Включить излучение
        /// </summary>
        public bool RadiationOn()
        {
            return CommanPartForCodograms(5, (byte)CodogramCode.RADIATION_ON, null, null);
        }


        /// <summary>
        /// Отключить излучение
        /// </summary>
        public bool RadiationOff()
        {
            return CommanPartForCodograms(5, (byte)CodogramCode.RADIATION_OFF, null, null);
        }

        /// <summary>
        /// Установить уровень мощности
        /// </summary>
        public bool SetPowerLevel(PowerLevelMessage data)
        {
            return CommanPartForCodograms(6, (byte)CodogramCode.SET_POWER_LVL,
                delegate (ref byte[] ForSend)
                {
                    ForSend[5] = (byte)data.PowerCode;
                },
                null);
        }


        ///// <summary>
        ///// Установить уровень мощности
        ///// </summary>
        //public bool SetPowerLevel(PowerCode powerCode)
        //{
        //    return CommanPartForCodograms(6, (byte)CodogramCode.SET_POWER_LVL,
        //        delegate (ref byte[] ForSend)
        //        {
        //            ForSend[5] = (byte)powerCode;
        //        },
        //        null);
        //}


        /// <summary>
        /// Запрос состояния   
        /// </summary>
        public bool GetState()
        {
            return CommanPartForCodograms(5, (byte)CodogramCode.GET_STATE, null, null);
        }


        /// <summary>
        /// Запрос отказов
        /// </summary>
        public bool GetFailure()
        {
            return CommanPartForCodograms(5, (byte)CodogramCode.GET_FAILURE, null, null);
        }


        /// <summary>
        /// ТУ Фильтр
        /// </summary>
        public bool SetAmplifierFilter(FilterMessage data)
        {
            return CommanPartForCodograms(6, (byte)CodogramCode.SET_AMP_FILTER,
                delegate (ref byte[] ForSend)
                {
                    ForSend[5] = (byte)data.Filter;
                },
                null);
        }

        ///// <summary>
        ///// ТУ Фильтр
        ///// </summary>
        //public bool SetAmplifierFilter(Filter filter)
        //{
        //    return CommanPartForCodograms(6, (byte)CodogramCode.SET_AMP_FILTER,
        //        delegate (ref byte[] ForSend)
        //        {
        //            ForSend[5] = (byte)filter;
        //        },
        //        null);
        //}


        /// <summary>
        /// Запрос Телесигнализации
        /// </summary>
        public bool GetTelesignalization()
        {
            return CommanPartForCodograms(5, (byte)CodogramCode.GET_TELESIGNALIZATION, null, null);
        }
        #endregion
    }
}
