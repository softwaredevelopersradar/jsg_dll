﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Media;
using PackageJSG;

namespace TestClientJSG
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private string filepath = ""; //путь к файлу
        private int myport;   //порт этого устройства
        private int fpsport;  //порт ФПС
        private string myIP;  //IP этого устройства
        private string fpsIP; //IP ФПС
        private int frequency;
        private int device1;
        //private JSG jsg;
        JsgManager clientVereskManager;

        string text = "";
        public string TextMess
        {
            get { return text; }
            set
            {
                text = value;
                PropertyChanged(this, new PropertyChangedEventArgs("TextMess"));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }

        public MainWindow()
        {
            DataContext = this;
            InitializeComponent();
            clientVereskManager = new JsgManager();
            SubscribeToManagerEvents();
            //var jsg = new JSG(1, "127.0.0.1", 13, "127.0.0.1", 3);
            //jsg.SetAmplifierFilter( new FilterMessage() { Filter = PackageJSG.Filter.High } );
        }

        private void SubscribeToManagerEvents()
        {
            clientVereskManager.OnMessToMain += ClientManager_OnMessToMain;
            clientVereskManager.OnConnected += ClientManager_OnConnected;
            clientVereskManager.OnDisconnected += ClientManager_OnDisconnected;
        }

        private void ClientManager_OnDisconnected(object sender, EventArgs e)
        {
            DispatchIfNecessary(() =>
            {
                btnConnect.Background = Brushes.Red;
            });

            //throw new NotImplementedException();
        }

        private void ClientManager_OnConnected(object sender, EventArgs e)
        {
            DispatchIfNecessary(() =>
            {
                btnConnect.Background = Brushes.Green;
            });
            //throw new NotImplementedException();
        }

        private void ClientManager_OnMessToMain(object sender, string e)
        {
            TextMess += e + "\n" + "\n";
        }

        private void BtnConnect_Click(object sender, RoutedEventArgs e)
        {
            myport = Convert.ToInt32(textMyPort.Text);
            fpsport = Convert.ToInt32(textFpsPort.Text);
            myIP = textMyIP.Text;
            fpsIP = textFpsIP.Text;

            clientVereskManager.Connect(myport, myIP, fpsport, fpsIP);
        }

        private void BtnDisconnect_Click(object sender, RoutedEventArgs e)
        {
            clientVereskManager.Disconnect();
        }

        private void BtnPath_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dialog = new Microsoft.Win32.OpenFileDialog();
            dialog.Filter = "WAVE Audio File (.wav)|*.wav";

            if (dialog.ShowDialog() == true)
            {
                filepath = dialog.FileName;   //указываем путь к файлу
                textPath.Text = dialog.FileName;
            }
        }

        private void BtnStartRecord_Click(object sender, RoutedEventArgs e)
        {
            frequency = Convert.ToInt32(textFreq.Text);
            //var source = new VoiceSourceMessage() { Filepath = filepath };
            //var control = new ControlVoiceMessage() { Regime = PackageJSG.VoiceRegime.Start, FrequencyKhz = frequency, Deviation = 1 };
            clientVereskManager.StartPlayRecord(filepath, frequency);
        }

        private void BtnStopRecord_Click(object sender, RoutedEventArgs e)
        {
            clientVereskManager.StopVoice(frequency);
        }

        private void BtnStartOnline_Click(object sender, RoutedEventArgs e)
        {
            frequency = Convert.ToInt32(textFreq.Text);
            device1 = Convert.ToInt32(textDevice.Text);
            byte device = Convert.ToByte(device1);
            clientVereskManager.StartLiveVoice(device, frequency);
        }

        private void BtnStopOnline_Click(object sender, RoutedEventArgs e)
        {
            clientVereskManager.StopVoice(frequency);
        }

        private void Btn_RadiatOff_Click(object sender, RoutedEventArgs e)
        {
            clientVereskManager.RadiationOff();
            //jsg.SetRadiatOff();
        }

        private void BtnDurFWS_Click(object sender, RoutedEventArgs e)
        {
            FFJammingMessage jam = new FFJammingMessage();
            jam.Duration = Convert.ToInt32(textDurat1.Text);
            jam.Params.Add(new JammingParamsMessage() { FrequencyKhz = Convert.ToInt32(textFreq1.Text), InterferenceParams = new InterferenceParamsMessage() { Modulation = Convert.ToInt32(textCodeModul1.Text), 
                Deviation = Convert.ToInt32(textCodeDeviat1.Text), Duration = Convert.ToInt32(textCodeDurat1.Text), Manipulation = Convert.ToInt32(textCodeManipul1.Text)}});
            clientVereskManager.SetParamsFFJamming(jam);
            //try
            //{
            //    InterferenceParam lib = new InterferenceParam
            //    {
            //        Modulation = 9,
            //        Deviation = 0,
            //        Manipulation = 0,
            //        Duration = 0
            //    };
            //    InterferenceParam lib1 = new InterferenceParam
            //    {
            //        Modulation = 1,
            //        Deviation = 1,
            //        Manipulation = 1,
            //        Duration = 1
            //    };
            //    TableSuppressFWS tDurationParamFWS = new TableSuppressFWS()
            //    {
            //        FreqKHz = 800000,
            //        InterferenceParam = lib
            //    };
            //    //TableSuppressFWS tDurationParamFWS1 = new TableSuppressFWS()
            //    //{
            //    //    FreqKHz = 5000000,
            //    //    InterferenceParam = lib1
            //    //};
            //    TableSuppressFWS tDurationParamFWS2 = new TableSuppressFWS()
            //    {
            //        FreqKHz = 3300000,
            //        InterferenceParam = lib1
            //    };
            //    List<TableSuppressFWS> list = new List<TableSuppressFWS>();
            //    list.Add(tDurationParamFWS);
            //    //list.Add(tDurationParamFWS1);
            //    list.Add(tDurationParamFWS2);
            //    fps.SetParamFWS(list, 500);
            //}
            //catch (Exception ex)
            //{
            //}
        }

        private void BtnFHSS_Click(object sender, RoutedEventArgs e)
        {
            clientVereskManager.OffsetFreqFHSS = Convert.ToInt32(textOffsetFreqFHSS.Text);
            FHSSJammingMessage jam = new FHSSJammingMessage();
            jam.Duration = Convert.ToInt32(textDurat1.Text);
            jam.CodeFFT = Convert.ToInt32(textCodeFFT.Text);
            jam.Params.Add(new JammingParamsMessage() { FrequencyKhz = Convert.ToInt32(textFreq1.Text),
                InterferenceParams = new InterferenceParamsMessage()
                {
                    Modulation = Convert.ToInt32(textCodeModul1.Text),
                    Deviation = Convert.ToInt32(textCodeDeviat1.Text),
                    Duration = Convert.ToInt32(textCodeDurat1.Text),
                    Manipulation = Convert.ToInt32(textCodeManipul1.Text)
                }
            });
            clientVereskManager.SetParamsFHSSJamming(jam);
        }

        private void BtnFHSSOff_Click(object sender, RoutedEventArgs e)
        {
            clientVereskManager.StopFHSS();
        }

        private void BtnJSG_State_Click(object sender, RoutedEventArgs e)
        {
            clientVereskManager.GetRegimeJSG();
        }

        private void BtnPowerOnAM_Click(object sender, RoutedEventArgs e)
        {
            clientVereskManager.PowerOnAM();
        }


        private void BtnPowerOffAM_Click(object sender, RoutedEventArgs e)
        {
            clientVereskManager.PowerOffAM();
        }

        private void BtnRadiatOnAM_Click(object sender, RoutedEventArgs e)
        {
            clientVereskManager.RadiationOnAM();
        }

        private void BtnRadiatOffAM_Click(object sender, RoutedEventArgs e)
        {
            clientVereskManager.RadiationOffAM();
        }


        private void BtnPowerLvlAM_Click(object sender, RoutedEventArgs e)
        {
            PowerLevelMessage data = new PowerLevelMessage() { PowerCode = PowerCode.P100 }; 
            clientVereskManager.SetPowerLevelAM(data);
        }

        private void BtnStateAM_Click(object sender, RoutedEventArgs e)
        {
            clientVereskManager.GetStateAM();
        }

        private void BtnStateFailureAM_Click(object sender, RoutedEventArgs e)
        {
            clientVereskManager.GetFailureAM();
        }

        private void BtnStateFilterAM_Click(object sender, RoutedEventArgs e)
        {
            FilterMessage data = new FilterMessage() { Filter = Filter.High };
            clientVereskManager.SetAmplifierFilterAM(data);
        }

        private void BtnTelesinalAM_Click(object sender, RoutedEventArgs e)
        {
            clientVereskManager.GetTelesignalizationAM();
        }


        public void DispatchIfNecessary(Action action)
        {
            if (!Dispatcher.CheckAccess())
                Dispatcher.Invoke(action);
            else
                action.Invoke();
        }

        private void BtnDurFWS2_Click(object sender, RoutedEventArgs e)
        {
            FFJammingMessage jam = new FFJammingMessage();
            jam.Duration = Convert.ToInt32(textDurat1.Text);
            jam.Params.Add(new JammingParamsMessage()
            {
                FrequencyKhz = Convert.ToInt32(textFreq1.Text),
                InterferenceParams = new InterferenceParamsMessage()
                {
                    Modulation = Convert.ToInt32(textCodeModul1.Text),
                    Deviation = Convert.ToInt32(textCodeDeviat1.Text),
                    Duration = Convert.ToInt32(textCodeDurat1.Text),
                    Manipulation = Convert.ToInt32(textCodeManipul1.Text)
                }
            });
            jam.Params.Add(new JammingParamsMessage()
            {
                FrequencyKhz = Convert.ToInt32(textFreq2.Text),
                InterferenceParams = new InterferenceParamsMessage()
                {
                    Modulation = Convert.ToInt32(textCodeModul2.Text),
                    Deviation = Convert.ToInt32(textCodeDeviat2.Text),
                    Duration = Convert.ToInt32(textCodeDurat2.Text),
                    Manipulation = Convert.ToInt32(textCodeManipul2.Text)
                }
            });
            clientVereskManager.SetParamsFFJamming(jam);
        }

        private void BtnFHSS2_Click(object sender, RoutedEventArgs e)
        {
            clientVereskManager.OffsetFreqFHSS = Convert.ToInt32(textOffsetFreqFHSS.Text);
            FHSSJammingMessage jam = new FHSSJammingMessage();
            jam.Duration = Convert.ToInt32(textDurat1.Text);
            jam.CodeFFT = Convert.ToInt32(textCodeFFT.Text);
            jam.Params.Add(new JammingParamsMessage()
            {
                FrequencyKhz = Convert.ToInt32(textFreq1.Text),
                InterferenceParams = new InterferenceParamsMessage()
                {
                    Modulation = Convert.ToInt32(textCodeModul1.Text),
                    Deviation = Convert.ToInt32(textCodeDeviat1.Text),
                    Duration = Convert.ToInt32(textCodeDurat1.Text),
                    Manipulation = Convert.ToInt32(textCodeManipul1.Text)
                }
            });
            jam.Params.Add(new JammingParamsMessage()
            {
                FrequencyKhz = Convert.ToInt32(textFreq2.Text),
                InterferenceParams = new InterferenceParamsMessage()
                {
                    Modulation = Convert.ToInt32(textCodeModul2.Text),
                    Deviation = Convert.ToInt32(textCodeDeviat2.Text),
                    Duration = Convert.ToInt32(textCodeDurat2.Text),
                    Manipulation = Convert.ToInt32(textCodeManipul2.Text)
                }
            });
            clientVereskManager.SetParamsFHSSJamming(jam);
        }
    }
}
