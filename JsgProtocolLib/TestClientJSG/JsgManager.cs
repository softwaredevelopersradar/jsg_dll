﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JsgLib;
using PackageJSG;

namespace TestClientJSG
{
    public class JsgManager
    {
        private JSG _client { get; set; }
        public bool IsConnected { get; private set; }
        public int OffsetFreqFHSS { get; set; } = 10000; //для ППРЧ отнимать от min частоты

        public event EventHandler<string> OnMessToMain;
        public event EventHandler OnDisconnected;
        public event EventHandler OnConnected;
        public JsgManager()
        {
            //_client = new JSG();
        }

        #region Commands
        public void Connect(int myPort, string myIP, int fpsPort, string fpsIP)
        {
            if (IsConnected)
                return;

            _client = new JSG(myPort, myIP, fpsPort, fpsIP, 3);
            SubscribeToLibEvents();
            _client?.Connect();
        }


        public void Disconnect()
        {
            if (!IsConnected)
                return;

            _client?.Disconnect();

        }

        public void StartPlayRecord(string filepath, int frequency, int deviation = 1)
        {
            if (!IsConnected)
                return;

            var source = new VoiceSourceMessage() { Filepath = filepath };
            var control = new ControlVoiceMessage() { Regime = PackageJSG.VoiceRegime.Start, FrequencyKhz = frequency, Deviation = deviation };
            if (_client.SetVoice(source))
            { _client?.RegimeVoice(control); }
        }

        public void StopVoice(int frequency, int deviation = 1)
        {
            if (!IsConnected)
                return;

            var control = new ControlVoiceMessage() { Regime = PackageJSG.VoiceRegime.Pause, FrequencyKhz = frequency, Deviation = deviation };
            _client?.RegimeVoice(control); 
        }

        public void StartLiveVoice(int device, int frequency, int deviation = 1)
        {
            if (!IsConnected)
                return;

            var source = new VoiceSourceMessage() { DeviceNumber = device };
            var control = new ControlVoiceMessage() { Regime = PackageJSG.VoiceRegime.Start, FrequencyKhz = frequency, Deviation = 1 };
            if (_client.SetVoice(source))
            { _client?.RegimeVoice(control); }
        }

        public void RadiationOff()
        {
            if (!IsConnected)
                return;

            _client?.SetJsgRadiationOff();
        }

        public void SetParamsFFJamming(FFJammingMessage data)
        {
            if (!IsConnected)
                return;

            _client?.SetParamFFJam(data);
        }

        public void SetParamsFHSSJamming(FHSSJammingMessage data)
        {
            if (!IsConnected)
                return;
            _client.OffsetFreqFHSS = OffsetFreqFHSS;
            _client?.SetParamFHSSJam(data);
        }

        public void StopFHSS()
        {
            if (!IsConnected)
                return;

            _client?.StopFHSS();
        }

        public void GetRegimeJSG()
        {
            if (!IsConnected)
                return;

            _client?.GetRegimeJSG();
        }

        /// <summary>
        /// Включить питание
        /// </summary>
        public void PowerOnAM()
        {
            if (!IsConnected)
                return;

            _client?.PowerOn();
        }


        /// <summary>
        /// Отключить питание
        /// </summary>
        public void PowerOffAM()
        {
            if (!IsConnected)
                return;

            _client?.PowerOff();
        }


        /// <summary>
        /// Включить излучение
        /// </summary>
        public void RadiationOnAM()
        {
            if (!IsConnected)
                return;

            _client?.RadiationOn();
        }


        /// <summary>
        /// Отключить излучение
        /// </summary>
        public void RadiationOffAM()
        {
            if (!IsConnected)
                return;

            _client?.RadiationOff();
        }

        public void SetPowerLevelAM(PowerLevelMessage data)
        {
            if (!IsConnected)
                return;

            _client?.SetPowerLevel(data);
        }


        public void GetStateAM()
        {
            if (!IsConnected)
                return;

            _client?.GetState();
        }


        /// <summary>
        /// Запрос отказов
        /// </summary>
        public void GetFailureAM()
        {
            if (!IsConnected)
                return;

            _client?.GetFailure();
        }


        /// <summary>
        /// ТУ Фильтр
        /// </summary>
        public void SetAmplifierFilterAM(FilterMessage data)
        {
            if (!IsConnected)
                return;

            _client?.SetAmplifierFilter(data);
        }



        /// <summary>
        /// Запрос Телесигнализации
        /// </summary>
        public void GetTelesignalizationAM()
        {
            if (!IsConnected)
                return;

            _client?.GetTelesignalization();
        }
        #endregion


        public void SubscribeToLibEvents()
        {
            _client.OnConnect += _client_OnConnect;
            _client.OnDisconnect += _client_OnDisconnect;
            _client.OnReceivedJsgCmd += _client_OnReceivedJsgCmd;
            _client.OnRecivedStateJSG += _client_OnRecivedStateJSG;
            _client.OnReceivedAmCmd += _client_OnReceivedAmCmd;
            _client.OnReceivedStateAM += _client_OnReceivedStateAM; 
            _client.OnReceivedFailureAm += _client_OnReceivedFailureAm;
            _client.OnReceivedTelesignalAmCmd += _client_OnReceivedTelesignalAmCmd;
            //_client.OnWriteBytes += _client_OnWriteBytes;
            //_client.OnReadBytes += _client_OnReadBytes;
            
            ////_client.Logger += ShowClientLogger;
            //_client.OnGetOperationsInstructionsReport += _client_OnGetOperationsInstructionsReport;
        }

        private void _client_OnReceivedTelesignalAmCmd(object sender, TelesignalResponseMessage e)
        {
            OnMessToMain(sender, $"Failure: {e.Failure}" +
                $" Ready : {e.IsReady}," +
                $" IsWorking : {e.IsWorking},");
        }

        private void _client_OnReceivedFailureAm(object sender, FailureAmResponseMessage e)
        {
            string errors = "";
            foreach (var t in e.FailureBitsNums)
                errors += ", " + t ;
            OnMessToMain(sender, $"Code: {e.JsgError.CommandCode}" +
                $" Failure bits : {errors},");
        }

        private void _client_OnReceivedStateAM(object sender, StateAmResponseMessage e)
        {
            OnMessToMain(sender, $"HasPower: {e.HasPower}" +
                $" HasRadiation : {e.HasRadiation}," +
                $" IsLocalControl : {e.IsLocalControl}," +
                $" Setted2Filter : {e.Setted2Filter}," +
                $" HasFailure : {e.HasFailure}," +
                $" IsReady : {e.IsReady}," +
                $" PowerExceeds500Vt : {e.PowerExceeds500Vt}," +
                $" Temperature : {e.Temperature}," +
                $" VSWR : {e.VSWR}," +
                $" Power : {e.Power}");
        }

        private void _client_OnReceivedAmCmd(object sender, DefaulAmResponseMessage e)
        {
            OnMessToMain(sender, "Get AM reply code: " + e.JsgError.CommandCode + ", " +
              $" Errors : {e.JsgError.Errors.ToStringUtf8()},"+
              $" Result : {e.Result},"+
              $" Alarms (AM, Filter, Power ) : {e.AlarmInfo.AmplifierAlarm}, {e.AlarmInfo.FiltersAlarm}, {e.AlarmInfo.PowerAlarm}");
        }

        private void _client_OnRecivedStateJSG(object sender, StateJsgResponseMessage e)
        {
            string versions = "";
            foreach (var t in e.FirmwareVersion)
                versions += "v" + t.ToStringUtf8();
            OnMessToMain(sender, "Current mode: " + (e.Mode).ToString() + ", " +
                $" Versions : {versions},");
        }

        private void _client_OnReceivedJsgCmd(object sender, DefaulJsgResponseMessage e)
        {
            OnMessToMain(sender, "Get JSG reply code: " + e.CommandCode + ", " +
               $" Errors : {e.Errors.ToStringUtf8()},");
        }

        public void UnsubscribeFromLibEvents()
        {
            _client.OnConnect -= _client_OnConnect;
            _client.OnDisconnect -= _client_OnDisconnect;
            _client.OnReceivedJsgCmd -= _client_OnReceivedJsgCmd;
            _client.OnRecivedStateJSG -= _client_OnRecivedStateJSG;
            _client.OnReceivedAmCmd -= _client_OnReceivedAmCmd;
            _client.OnReceivedStateAM -= _client_OnReceivedStateAM;
            _client.OnReceivedFailureAm -= _client_OnReceivedFailureAm;
            _client.OnReceivedTelesignalAmCmd -= _client_OnReceivedTelesignalAmCmd;
        }

        #region EventHandlers

        private void _client_OnReadBytes(object sender, byte[] e)
        {
            OnMessToMain(sender, "Get " + e.Length + " bytes from server");
        }

        private void _client_OnWriteBytes(object sender, byte[] e)
        {
            OnMessToMain(sender, "Send " + e.Length + " bytes to server");
        }

        private void _client_OnDisconnect(object sender, EventArgs e)
        {
            IsConnected = false;
            OnMessToMain(sender, "Closed UDP port");
            OnDisconnected?.Invoke(this, null);
            UnsubscribeFromLibEvents();
            _client = null;
        }

        private void _client_OnConnect(object sender, EventArgs e)
        {
            IsConnected = true;
            OnMessToMain(sender, "Opened UDP port");
            OnConnected?.Invoke(this, null);
        }


        #endregion

    } 
}
